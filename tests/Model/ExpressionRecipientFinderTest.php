<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace Model;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use SymfonyBro\NotificationCore\Model\ContextInterface;
use SymfonyBro\NotificationExpressionBridge\Model\ConditionAwareInterface;
use SymfonyBro\NotificationExpressionBridge\Model\ExpressionRecipientFinder;

class ExpressionRecipientFinderTest extends TestCase
{
    public function testFind()
    {
        $expressionLanguage = new ExpressionLanguage();

        $recipients = [
            new class implements ConditionAwareInterface {
                public function getCondition(): string
                {
                    return '1 > x';
                }
            },
            new class implements ConditionAwareInterface {
                public function getCondition(): string
                {
                    return '1 < x';
                }
            },
        ];

        $context = new class implements ContextInterface {};

        /** @var ExpressionRecipientFinder|MockObject $finder */
        $finder = $this->getMockBuilder(ExpressionRecipientFinder::class)
            ->setMethods(['doFind', 'getExpressionContext'])
            ->setConstructorArgs([$expressionLanguage])
            ->getMockForAbstractClass();

        $finder->expects($this->once())
            ->method('doFind')
            ->willReturn($recipients);

        $finder->expects($this->once())
            ->method('getExpressionContext')
            ->with($context)
            ->willReturn(['x' => 2]);

        $recipients = $finder->find($context);

        $this->assertCount(1, $recipients);
    }
}