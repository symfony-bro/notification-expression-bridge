<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace Model;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use SymfonyBro\NotificationCore\Model\ContextInterface;
use SymfonyBro\NotificationCore\Model\RecipientInterface;
use SymfonyBro\NotificationExpressionBridge\Model\GroupInterface;
use SymfonyBro\NotificationExpressionBridge\Model\GroupRecipientFinder;

class GroupRecipientFinderTest extends TestCase
{
    public function testFind()
    {
        $expressionLanguage = new ExpressionLanguage();

        $groups = [
            new class implements GroupInterface {

                public function getGroupCondition(): string
                {
                    return '1 > x';
                }

                public function getRecipientCondition(): string
                {
                    return '1 > x';
                }
            },
            new class implements GroupInterface {

                public function getGroupCondition(): string
                {
                    return '1 < x';
                }

                public function getRecipientCondition(): string
                {
                    return '1 < x';
                }
            },
        ];

        $recipients = [
            new class implements RecipientInterface {

            },
            new class implements RecipientInterface {

            },
        ];

        $context = new class implements ContextInterface {};

        /** @var GroupRecipientFinder|MockObject $finder */
        $finder = $this->getMockBuilder(GroupRecipientFinder::class)
            ->setMethods(['findGroups', 'getExpressionContext', 'findRecipients', 'getRecipientExpressionContext'])
            ->setConstructorArgs([$expressionLanguage])
            ->getMockForAbstractClass();

        $finder->expects($this->once())
            ->method('getExpressionContext')
            ->with($context)
            ->willReturn(['x' => 2]);

        $finder->expects($this->once())
            ->method('findGroups')
            ->with($context)
            ->willReturn($groups);

        $finder->expects($this->once())
            ->method('findRecipients')
            ->with($groups[1])
            ->willReturn($recipients);

        $finder->expects($this->exactly(\count($recipients)))
            ->method('getRecipientExpressionContext')
            ->with($context)
            ->willReturn(['x' => 2]);

        $recipients = $finder->find($context);

        $this->assertCount(2, $recipients);
    }
}