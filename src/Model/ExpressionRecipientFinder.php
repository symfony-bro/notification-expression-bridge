<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\NotificationExpressionBridge\Model;


use function array_filter;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use SymfonyBro\NotificationCore\Model\ContextInterface;
use SymfonyBro\NotificationCore\Model\RecipientFinderInterface;
use SymfonyBro\NotificationCore\Model\RecipientInterface;

abstract class ExpressionRecipientFinder implements RecipientFinderInterface
{
    /**
     * @var ExpressionLanguage
     */
    private $expressionLanguage;

    /**
     * ExpressionRecipientFinder constructor.
     * @param ExpressionLanguage $expressionLanguage
     */
    public function __construct(ExpressionLanguage $expressionLanguage)
    {
        $this->expressionLanguage = $expressionLanguage;
    }

    /**
     * @param ContextInterface $context
     * @return RecipientInterface[]
     */
    public function find(ContextInterface $context): array
    {
        $recipients = $this->doFind($context);
        $expressionContext = $this->getExpressionContext($context);
        return array_filter($recipients, function ($recipient) use ($expressionContext) {
            if ($recipient instanceof ConditionAwareInterface) {
                return $this->expressionLanguage->evaluate($recipient->getCondition(), $expressionContext);
            }

            return false; // TODO - or true?
        });
    }

    abstract protected function doFind(ContextInterface $context): array;

    abstract protected function getExpressionContext(ContextInterface $context): array;
}