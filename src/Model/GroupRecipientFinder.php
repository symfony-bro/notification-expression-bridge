<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\NotificationExpressionBridge\Model;


use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use SymfonyBro\NotificationCore\Model\ContextInterface;
use SymfonyBro\NotificationCore\Model\RecipientFinderInterface;
use SymfonyBro\NotificationCore\Model\RecipientInterface;

abstract class GroupRecipientFinder implements RecipientFinderInterface
{
    /**
     * @var ExpressionLanguage
     */
    private $expressionLanguage;

    /**
     * ExpressionRecipientFinder constructor.
     * @param ExpressionLanguage $expressionLanguage
     */
    public function __construct(ExpressionLanguage $expressionLanguage)
    {
        $this->expressionLanguage = $expressionLanguage;
    }

    /**
     * @param ContextInterface $context
     * @return RecipientInterface[]
     */
    public function find(ContextInterface $context): array
    {
        $expressionContext = $this->getExpressionContext($context);

        /** @var GroupInterface[] $groups */
        $groups = \array_filter($this->findGroups($context), function (GroupInterface $conditionAware) use ($expressionContext) {
            return $this->expressionLanguage->evaluate($conditionAware->getGroupCondition(), $expressionContext);
        });

        $recipients = [];
        foreach ($groups as $group) {
            $recipients[] = \array_filter($this->findRecipients($group), function (RecipientInterface $recipient) use ($group, $context) {
                $recipientContext = $this->getRecipientExpressionContext($context, $recipient);
                return $this->expressionLanguage->evaluate($group->getRecipientCondition(), $recipientContext);
            });
        }

        return $this->unique(\array_merge(...$recipients));
    }

    /**
     * @param ContextInterface $context
     * @return GroupInterface[]
     */
    abstract protected function findGroups(ContextInterface $context): array;

    /**
     * @param ContextInterface $context
     * @return array
     */
    abstract protected function getExpressionContext(ContextInterface $context): array;

    /**
     * @param GroupInterface $group
     * @return RecipientInterface[]
     */
    abstract protected function findRecipients(GroupInterface $group): array;

    /**
     * @param ContextInterface $context
     * @param RecipientInterface $recipient
     * @return array
     */
    abstract protected function getRecipientExpressionContext(ContextInterface $context, RecipientInterface $recipient): array;

    private function unique(array $elements)
    {
        $result = [];
        foreach ($elements as $element) {
            if (!\in_array($element, $result, true)) {
                $result[] = $element;
            }
        }

        return $result;
    }

}