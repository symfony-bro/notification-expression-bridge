<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\NotificationExpressionBridge\Model;


interface GroupConditionAwareInterface
{
    public function getGroupCondition(): string;
}