<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\NotificationExpressionBridge\Model;


class AbstractGroup implements GroupConditionAwareInterface, RecipientConditionAwareInterface
{
    /**
     * @var string
     */
    private $groupCondition;

    /**
     * @var string
     */
    private $recipientCondition;

    /**
     * AbstractGroup constructor.
     * @param string $groupCondition
     * @param string $recipientCondition
     */
    public function __construct(string $groupCondition, string $recipientCondition)
    {
        $this->groupCondition = $groupCondition;
        $this->recipientCondition = $recipientCondition;
    }

    public function getGroupCondition(): string
    {
        return $this->groupCondition;
    }

    public function getRecipientCondition(): string
    {
        return $this->recipientCondition;
    }
}